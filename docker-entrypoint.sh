#!/bin/bash

# When not limiting the open file descriptors limit, the memory consumption of
# slapd is absurdly high. See https://github.com/docker/docker/issues/8231
ulimit -n 8192

set -euo pipefail

if [[ ! -d /etc/ldap/slapd.d ]]
then
	echo "Config initialisation..."

	# Move back ldap config.
	mv /etc/ldap.bak/* /etc/ldap

	# Exit if we don't have PASSWORD or DOMAIN
	if [[ -z "$SLAPD_PASSWORD" ]]
	then
		echo -n >&2 "Error: Container not configured and SLAPD_PASSWORD not set. "
		echo >&2 "Did you forget to add -e SLAPD_PASSWORD=... ?"
		exit 1
	fi

	if [[ -z "$SLAPD_DOMAIN" ]]
	then
		echo -n >&2 "Error: Container not configured and SLAPD_DOMAIN not set. "
		echo >&2 "Did you forget to add -e SLAPD_DOMAIN=... ?"
		exit 1
	fi

	SLAPD_ORGANIZATION="${SLAPD_ORGANIZATION:-${SLAPD_DOMAIN}}"

	cat <<-EOF | debconf-set-selections
		slapd slapd/password1 password $SLAPD_PASSWORD
		slapd slapd/password2 password $SLAPD_PASSWORD
		slapd shared/organization string $SLAPD_ORGANIZATION
		slapd slapd/domain string $SLAPD_DOMAIN
EOF

	# Finish slapd installation since it was aborted during image build
	echo "Reconfiguring slapd..."
	dpkg-reconfigure -f noninteractive slapd
fi

# Load pre-population .ldif files
if [[ -d "/prepopulate" ]]
then
	mkdir -p /etc/ldap/prepopulate
	for file in /prepopulate/*.ldif
	do
		[[ -e "$file" ]] || break  # handle the case of no *.ldif files
		if [[ ! -f "/etc/ldap$file.done" ]]
		then
			# Load the .ldif file and create /etc/ldap/prepopulate/$file.done to mark it as loaded
			echo "Loading populate: $file"
			slapadd -F /etc/ldap/slapd.d -l "$file"
			touch "/etc/ldap$file.done"
		fi
	done
fi

# Gives ownership to the openldap user.
chown -R openldap:openldap /etc/ldap/ /var/lib/ldap/ /var/run/slapd/
# Start ldap in background to allow ldapmodify to connect.
slapd -u openldap -g openldap -h "ldapi:///"

# Load config .ldif files.
if [[ -d "/config" ]]
then
	mkdir -p /etc/ldap/config

	for file in /config/*.ldif
	do
		[[ -e "$file" ]] || break  # handle the case of no *.ldif files
		if [[ ! -f "/etc/ldap$file.done" ]]
		then
			# Load the .ldif file and create /etc/ldap/config/$file.done to mark it as loaded.
			echo "Loading config: $file"
			ldapmodify -Y EXTERNAL -H ldapi:/// -f "$file"
			touch "/etc/ldap$file.done"
		fi
	done
fi

# Load schemas .ldif files.
if [[ -d "/schemas" ]]
then
	mkdir -p /etc/ldap/schemas
	for file in /schemas/*.ldif
	do
		[[ -e "$file" ]] || break  # handle the case of no *.ldif files
		if [[ ! -f "/etc/ldap$file.done" ]]
		then
			# Load the .ldif file and create /etc/ldap/schemas/$file.done to mark it as loaded.
			echo "Loading schema: $file"
			ldapadd -Y EXTERNAL -H ldapi:/// -f "$file"
			touch "/etc/ldap$file.done"
		fi
	done
fi

# Kill ldap server.
slap_pid=$(cat /var/run/slapd/slapd.pid)
kill -s TERM "$slap_pid"
# Wait for slapd to be terminated.
while [[ -d /proc/$slap_pid ]]
do
	sleep 1
done

# Gives ownership to the openldap user.
chown -R openldap:openldap /etc/ldap/ /var/lib/ldap/ /var/run/slapd/

echo "Starting slapd..."
# -d is debug level: https://www.openldap.org/doc/admin24/slapdconfig.html
# -u/g starts ldap as user and group 'openldap'
# -h server over the network and over a file socket.
exec slapd -d 256 -u openldap -g openldap -h "ldap:/// ldapi:///"
