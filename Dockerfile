# Inspried by https://github.com/dinkel/docker-openldap
FROM debian:11-slim

VOLUME ["/var/lib/ldap", "/etc/ldap"]

# Prevent apt from asking information during ldap installation
ENV DEBIAN_FRONTEND noninteractive

ENV OPENLDAP_VERSION 2.4.57
ENV FLAP_VERSION 3

# mkdir fixes gitlab auto devop build
RUN mkdir -p /var/run && \
    apt-get update && \
    # Install slapd
    apt-get install --no-install-recommends -y slapd=${OPENLDAP_VERSION}* ldap-utils && \
    # Move the ldap conf to prevent docker volumes from overriding it.
    # It will be moved back on the first run by docker-entrypoint.sh.
    mkdir /etc/ldap.bak && \
    mv /etc/ldap/* /etc/ldap.bak/ && \
    # Reduce final image size.
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./docker-entrypoint.sh /docker-entrypoint.sh

CMD ["/docker-entrypoint.sh"]
